<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\DataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('test', function (Request $request) {
    return response()->json([ 'android' => "2.0.0", 'ios' => '2.6.8', 'force_update' => false]);
});

Route::get('/get_customer_order_details',[DataController::class,'getCustomerOrderDetails'])->name('getCustomerOrderDetails');
Route::get('/get_top_customers',[DataController::class,'getTopCustomers'])->name('getTopCustomers');
Route::get('/get_top_products',[DataController::class,'getTopProducts'])->name('getTopProducts');
