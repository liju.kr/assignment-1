<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    use HasFactory;

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'orderNumber');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'productCode', 'productCode');
    }
}
