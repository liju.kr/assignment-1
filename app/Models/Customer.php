<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customerNumber', 'customerNumber');
    }


    public function order_details() {
        return $this->hasManyThrough(Orderdetail::class,  Order::class, 'customerNumber', 'orderNumber', 'customerNumber', 'orderNumber');
    }


    public function purchased_products() {
        return $this->hasManyDeep(Product::class, [Order::class, Orderdetail::class],['customerNumber', 'orderNumber', 'productCode'], ['customerNumber', 'orderNumber', 'productCode']);
    }


}
