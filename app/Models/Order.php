<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function order_detail()
    {
        return $this->hasOne('App\Models\Orderdetail', 'orderNumber', 'orderNumber');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customerNumber', 'customerNumber');
    }
}
