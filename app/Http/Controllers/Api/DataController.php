<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DataController extends Controller
{
    public function getCustomerOrderDetails(Request $request)
    {

        try{
        $validator =  Validator::make($request->all(), [
            'customerNumber' => 'required|numeric',
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status_message' => 'validation_error',
                'message' => "Validation Error",
                'errors' => $validator->errors(),
                'success' => false
            ]);

        }

            $customer = Customer::where('customerNumber', $request->customerNumber)->first();
            if($customer)
            {
                $customer_orders = Order::with('order_detail', 'customer')
                    ->where('customerNumber', $request->customerNumber)
                    ->paginate(100);

                return response()->json([
                    'status_message' => 'success',
                    'message' => "Success",
                    'customer_order_details' => $customer_orders,
                    'success' => true
                ]);
            }
            else
            {
                return response()->json([
                    'status_message' => 'no_results_found',
                    'message' => "Customer Number does not exist",
                    'success' => false
                ]);
            }
        } catch(\Exception $e){

            return response()->json([
                'status_message' => 'server_error',
                'message' => $e->getMessage(),
                'success' => false
            ]);
        }

    }

    public function getTopCustomers(Request $request)
    {

        try{

            $customers = Customer::withCount('orders')
                                         ->withSum('order_details','quantityOrdered')
                                         //->withSum('order_details','priceEach')
                                         ->orderBy('purchased_products_count', 'DESC')
                                         ->orderBy('order_details_sum_quantity_ordered', 'DESC')
                                         ->withCount('purchased_products')
                                         ->limit(10)->get();

            if($customers->count())
            {
                return response()->json([
                    'status_message' => 'success',
                    'message' => "Success",
                    'top_customers' => $customers,
                    'success' => true
                ]);
            }
            else
            {
                return response()->json([
                    'status_message' => 'no_results_found',
                    'message' => "No Customers Found",
                    'success' => false
                ]);
            }
        } catch(\Exception $e){

            return response()->json([
                'status_message' => 'server_error',
                'message' => $e->getMessage(),
                'success' => false
            ]);
        }

    }

    public function getTopProducts(Request $request)
    {

        try{

            $products = Product::orderBy(\DB::raw("`MSRP` - `buyPrice`"), 'DESC')->limit(10)->get();

            if($products->count())
            {
                return response()->json([
                    'status_message' => 'success',
                    'message' => "Success",
                    'top_products' => $products,
                    'success' => true
                ]);
            }
            else
            {
                return response()->json([
                    'status_message' => 'no_results_found',
                    'message' => "No Customers Found",
                    'success' => false
                ]);
            }
        } catch(\Exception $e){

            return response()->json([
                'status_message' => 'server_error',
                'message' => $e->getMessage(),
                'success' => false
            ]);
        }

    }



}
